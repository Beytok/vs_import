<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

IncludeModuleLangFile(__FILE__);

CModule::IncludeModule("main");
CModule::IncludeModule("iblock");
CModule::IncludeModule("ws_import");

$APPLICATION->SetTitle(GetMessage("TITLE"));

if($_REQUEST["mode"] == "list"){
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");
}else{
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
}
$rsSites = CSite::GetByID(CMainPage::GetSiteByHost());
$arSite = $rsSites->Fetch();
 ini_set('display_errors', 1);
$arResult=WsImport::Action();

$adminPage->ShowSectionIndex("global_menu_ws_import");
?>
<script type="text/javascript">

	function Reload() {
		window.location.reload();
	}

	<? if($arResult['RESULT']['RELOAD_PAGE']=='1'){?>
		setTimeout('Reload()', '2500');
	<?}?>
</script>

<div id="info_continer">
	<div class="step_block">
		<?=ShowNote(GetMessage('STEP').' '.$arResult['DATA']['STEP'].(($arResult['DATA']['SUB_STEP']>0)?' ('.GetMessage('POSITION').' '.$arResult['DATA']['SUB_STEP'].') '.GetMessage('PLEASE_WAIT'):''));?>
	</div>
	<div class="messages">
		<?
		foreach($arResult['RESULT']['WS_MESSAGE'] as $MM) { ?>
			<?=GetMessage($MM);?>
		<? } ?>
		<? foreach($arResult['RESULT']['MESSAGE'] as $MM) { ?>
			<?=ShowNote($MM);?>
		<? } ?>
	</div>
	<? if(is_array($arResult['PARAMS']['ERRORS'])) { ?>
		<div class="errors">
			<? foreach($arResult['PARAMS']['ERRORS'] as $ERROR) {?>
				<p class="error"><?=ShowMessage($ERROR);?></p>
			<?}?>
			<? foreach($arResult['DATA']['WARNINGS'] as $WARNING) {?>
				<p class="error"><?=ShowMessage($WARNING);?></p>
			<?}?>
			<form action="" method="POST">
				<p>
					<label for="conf_errors">
						<input type="checkbox" id="conf_errors" name="WS_IMPORT[<?=$arResult['PARAMS']['IBLOCK_ID'];?>][CONFIRM_WITH_ERRORS]">
						<?=GetMessage('GO_NEXT');?>
					</label>
				</p>
				<p><input type="submit" name="submit" value="<?=GetMessage('DO_NEXT');?>"></p>

			</form>
		</div>
	<? } else {
		if($arResult['DATA']['STEP']<'1' && !is_array($arResult['MESSAGE'])) { ?>
			<?=ShowMessage(GetMessage('FILE_UPLOAD_INFO'));?>
			<form enctype="multipart/form-data" method="POST">
				<p><input type="file" name="import_file"></p>
				<p><input type="submit" name="submit" value="<?=GetMessage('START');?>"></p>
			</form>
	<? }
	} ?>
</div>
<?
if($_REQUEST["mode"] == "list")
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_js.php");
else
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");


require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");
?>
