<?
IncludeModuleLangFile(__FILE__);

AddEventHandler("main", "OnBuildGlobalMenu", "AddGlobalMenuItem");
function AddGlobalMenuItem() {
  $arr = array(
    "global_menu_ws_import" => array(
    "icon" => "button_websites_support",
		"page_icon" => "page_websites_support",
		"index_icon" => "index_websites_support",
		"text" => 'Импорт WS',
		"title" => 'Импорт WS',
		"url" => "ws_import.php",
		"sort" => 500,
		"items_id" => "global_menu_websites_support",
		"help_section" => "settings",
		"items" => array()
	));
  
  return $arr;
}
$aMenu[] = array(
	"parent_menu" => "global_menu_ws_import",
	"section" => "menu_ws_import",
	"sort" => 100,
	"url" => "ws_import.php",
	"text" => 'Импорт WS',
	"title" => 'Импорт WS',
	"icon" => "websites_support_menu_icon_clients",
	"page_icon" => "websites_support_page_icon_clients",
	"module_id" => "ws_messenger",
	"items_id" => "menu_ws_messenger_messenger",
);
return $aMenu;
?>