<?
$MESS['TITLE']='Импорт WS';
$MESS['STEP']='Шаг';
$MESS['POSITION']='Позиция';

$MESS['JUST_EXPORT']='Только получить файл export.csv';
$MESS['CHANGE_NAME']='Обновить названия';

$MESS['PLEASE_WAIT']='Пожалуйста, ожидайте...';

$MESS['DOWNLOAD_EXPORT']='Скачать export.csv <a target="_blank" href="/upload/import_ws/export.csv">Export.csv</a>';

$MESS['FILE_FORMAT_EXAMPLE']='Скачать пример файла прайс-листа <a target="_blank" href="/upload/import_ws/import_example.csv">import_example.csv</a>. Разделитель полей "|".';
?>
