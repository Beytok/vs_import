<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

global $DB, $MESS, $APPLICATION;

if (!class_exists('CMainPage')) {
	require($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/mainpage.php");
	CModule::IncludeModule("main");
}

CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");

$rsSites = CSite::GetByID(CMainPage::GetSiteByHost());
$arSite = $rsSites->Fetch();

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/ws_import/lang/'.$arSite['LANGUAGE_ID'].'/classes/general/ws_import.php');

class WsImportTech{

	function charTranslit($value){
		$arParams = array("replace_space"=>"_","replace_other"=>"_", 'delete_repeat_replace'=>'true', 'max_len'=>'150', 'change_case'=>'L');
		return Cutil::translit($value, "ru", $arParams);
	}

	function GenerateAddInfo($arRes, $arParams, $what_generate='') {

		switch($what_generate) {
			case 'ELEMENT_CODE':
				return WsImportTech::charTranslit($arRes['ARTIKUL']);
			break;
			case 'CURRENCY':
				return $arParams['DEFAULT_CURRENCY'];
			break;
		}
	}

	function WriteToFile($file_name, $data_array, $write_params){
		if(is_array($data_array)) {
			$arParams=WsImport::Init();

			$h_new = fopen($file_name, $write_params);

			foreach($data_array as $new_line) {
				$string_data.=implode($arParams['DELIMITER'], $new_line);
				$string_data.="\n";
			}

			fwrite($h_new, $string_data);
			fclose($h_new);
			chmod($h_new, 0777);
		}
	}

	function CreateElementArray($arElement_array, $arParams) {

		$arResult = Array(
			'CODE'=>WsImportTech::GenerateAddInfo($arElement_array, $arParams, 'ELEMENT_CODE'),
			'XML_ID' => $arElement_array['ARTIKUL'],
			'IBLOCK_ID'=>$arParams['IBLOCK_ID'],
			'ACTIVE'=>'Y',
			'NAME'=>$arElement_array['NAME'],
			'PROPERTY_VALUES'=>$arElement_array['PROPERTY_VALUES'],
		);

		return $arResult;
	}

	function UpdateElement($element_id, $arRes_clean, $arParams) {

		CIBlockElement::SetPropertyValuesEx($element_id, $arParams['IBLOCK_ID'], $arRes_clean);

		//���������� ��� �������

		$arResult['OK']=1;
		$arResult['ID']=$element_id;
		return $arResult;
	}

	function AddElement($arRes_clean, $arParams) {

		$el = new CIBlockElement;

		if($res = $el->Add(WsImportTech::CreateElementArray($arRes_clean, $arParams))) {
			$arResult['OK']=1;
			$arResult['ID']=$res;
		}
		else {
			echo $error = $el->LAST_ERROR;
			WsImportTech::AddErrorRow($error, $arRes_clean, $arParams);
		}

		return $arResult;
	}

	function AddErrorRow($er, $arVal,$arParams,$moreText=''){

		if($arParams['LOG_FILE']!=""){
			$error = $er."\r\n";

			foreach($arVal as $key => $val){
				$error .= $key.":".$val."\r\n";
			}

			$error .= "\r\n".$moreText."\r\n";
			$error .= "--------------------------\r\n";

			$file = fopen($arParams['LOG_FILE'],"a");
			fwrite($file,$error);
			fclose($file);
		}
	}
}

Class WsImport {
	function Init($arParams='') {
		$arParams['IBLOCK_ID']=($arParams['IBLOCK_ID']=='')?WsImport::GetModuleParam('catalog_iblock_id'):$arParams['IBLOCK_ID'];
		$arParams['ARTICLE_ID']=(WsImport::GetModuleParam('catalog_article_id')=='')?WsImport::GetModuleParam('catalog_article_id'):470;
		$arParams['AVAILABLE_ID']=(WsImport::GetModuleParam('catalog_available_id')=='')?WsImport::GetModuleParam('catalog_available_id'):469;

		$arParams['LAST_EXPORT_TIME'] = WsImport::GetModuleParam('last_export_start_time');

		$arParams['DEFAULT_FOLDER'] = $_SERVER['DOCUMENT_ROOT'].'/upload/import_ws/';

		$arParams['CSV_FIELD_TYPE'] = 'R';

		$arParams['FILE_WITH_PARAMS'] = $arParams['DEFAULT_FOLDER'].'settings.txt';

		$arParams['DEFAULT_IMPORT_XML'] = $arParams['DEFAULT_FOLDER'].'import.xml';

		$arParams['LOG_FILE'] = $arParams['DEFAULT_FOLDER'].'log.txt';

		$arParams['COUNT_ONE_STEP_IMPORT_EL']=500;

		return $arParams;
	}

	function InitData($arParams, $addParams='', $clearParams='') {

		$arParams=(is_array($arParams))?$arParams:WsImport::Init();

		if(file_exists($arParams['FILE_WITH_PARAMS'])) {
			$arData=unserialize(file_get_contents($arParams['FILE_WITH_PARAMS']));
			if(!is_array($arData)){
				$arData=array();
			}
		}

		foreach($_REQUEST['WS_IMPORT'][$arParams['IBLOCK_ID']] as $name_v => $value) {
			$arData[$arParams['IBLOCK_ID']][$name_v]=$value;
		}

		if($clearParams=='all') {
			$arData[$arParams['IBLOCK_ID']]='';
			unset($arData);
		}
		elseif(is_array($clearParams)) {
			foreach($clearParams as $clear_key) {
				unset($arData[$arParams['IBLOCK_ID']][$clear_key]);
			}
		}

		if(is_array($addParams)) {
			foreach($addParams as $key_param => $value_param) {
				$arData[$arParams['IBLOCK_ID']][$key_param] = $value_param;
			}

			$add_it=1;
		}

		$h_new = fopen($arParams['FILE_WITH_PARAMS'], 'w');
		fwrite($h_new, serialize($arData));
		fclose($h_new);
		chmod($h_new, 0777);

		if(is_array($arData[$arParams['IBLOCK_ID']])) {
			foreach($arData[$arParams['IBLOCK_ID']] as $key_n => $value) {
				$arData[$key_n]=$value;
			}
		}
		return $arData;
	}

	function GetModuleParam($param_name){
		return 	COption::GetOptionString('ws_import', $param_name);
	}

	function SetModuleParam($param_name, $param_value){
		return COption::SetOptionString('ws_import', $param_name, $param_value);
	}

	function Action($arParams) {
		$arParams=WsImport::Init($arParams);
		$arData=WsImport::InitData($arParams);

		//checking exists step
		if(!method_exists('WsImport','Step'.$arData['STEP'])) {
			$arData['STEP']=0;
			WsImport::InitData(
				$arParams,
				array(
					'SUB_STEP'=>0,
					'STEP'=>$arData['STEP']
				)
			);
		}

		//checking global errors
		switch($arData['STEP']) {
			case '0':
				//check not had uncomplete import
				if($arParams['LAST_EXPORT_TIME']>0 && $arData['CONFIRM_WITH_ERRORS']!='on') {
					$arParams['ERRORS'][]=GetMessage('WAS_STARTED_EXPORT').date('H:i:s d.m.Y', $arParams['LAST_EXPORT_TIME']);
				}
			break;
		}

		//start all functions
		$func_name='Step'.$arData['STEP'];

		$arResult=WsImport::$func_name($arParams, $arData);

		if(is_array($arResult['WARNINGS'])) {
			if(is_array($arData['WARNINGS'])) {
				$arResult['WARNINGS']=array_merge($arData['WARNINGS'], $arResult['WARNINGS']);
			}

			WsImport::InitData(
				'',
				array(
					'WARNINGS'=>$arResult['WARNINGS']
				)
			);
		}

		if($arData['FINISH']=='Y' || $arResult['FINISH']=='Y') {
			WsImport::SetModuleParam('last_export_start_time', 0);
			WsImport::InitData('', '', 'all');
		}

		if(!is_array($arParams['ERRORS']) && $arData['FINISH']!='Y' && $arResult['FINISH']!='Y' && $arResult['WAIT_THIS']!='Y') {
			if(!$arResult['WAIT_THIS_STEP']) {
				WsImport::InitData(
					$arParams,
					array(
						'SUB_STEP'=>0,
						'STEP'=>($arData['STEP']+1)
					)
				);
			}
			$arResult['RELOAD_PAGE']=1;
		}

		return array(
			'PARAMS'=>$arParams,
			'DATA'=>WsImport::InitData($arParams),
			'RESULT'=>$arResult
		);
	}

	//get user file, get all info from user
	function Step0($arParams, $arData) {
		$arResult['WAIT_THIS']='Y';

		$uploadfile = $arParams['DEFAULT_IMPORT_XML'];

		if (copy($_FILES['import_file']['tmp_name'], $uploadfile)) {
			unset($arResult['WAIT_THIS']);
			WsImport::SetModuleParam('last_export_start_time', time()); //mark what we alredy had started import
			$arResult['MESSAGE'][] = GetMessage('FILE_UPLOADED');
		}
		/* Add */
		elseif (is_numeric($arParams["IBLOCK_ID"]) &&
		  $_REQUEST["WS_IMPORT"][$arParams["IBLOCK_ID"]]["GET_USER_EXPORT"] == 'on')
		{
	    unset($arResult['WAIT_THIS']);
			WsImport::SetModuleParam('last_export_start_time', time()); //mark what we alredy had started import
		}
		/* Add */

		return $arResult;
	}

	//create copy of db
	function Step1($arParams, $arData) {
		return $arResult;
	}

	//split uploaded file to exists and not exists elements
	function Step2($arParams, $arData) {
		$iblockId = $arParams['IBLOCK_ID'];
		$elementArticleId = $arParams['ARTICLE_ID'];
		$elementAvaibleId = $arParams['AVAILABLE_ID'];

		$xml = simplexml_load_string(file_get_contents($arParams['DEFAULT_IMPORT_XML']), 'SimpleXMLElement', LIBXML_NOCDATA);


		$num_el = $current_step_count_el = 0;

		$json = json_encode($xml);
		$array = json_decode($json,TRUE);
		$arrayGoods = $array['vendor']['goods'];

		if(count($array['vendor']) > 2){
			$arrayGoods = array();
			foreach($array['vendor'] as $goodsVendor => $vendor){
				$arrayGoods = array_merge($arrayGoods,$vendor['goods']);
			}
		}
		foreach($arrayGoods as $goodsKey => $goods){
			if(!is_numeric($goodsKey)){
				$arGoods[$arrayGoods['code']] = array(
					'NAME' => $arrayGoods['gname'],
					'PROPERTY_VALUES' => array(
						$elementArticleId => $arrayGoods['code'],
						$elementAvaibleId => '',
					)
				);
				if(!empty($arrayGoods['slq'])){
					$arGoods[$arrayGoods['code']]['PROPERTY_VALUES'][$elementAvaibleId] = $arrayGoods['slq'];
				}
				continue;
			}

			$arGoods[$goods['code']] = array(
				'NAME' => $goods['gname'],
				'PROPERTY_VALUES' => array(
					$elementArticleId => $goods['code'],
					$elementAvaibleId => '',
				)
			);
			if(!empty($goods['slq'])){
				$arGoods[$goods['code']]['PROPERTY_VALUES'][$elementAvaibleId] = $goods['slq'];
			}
		}

		$el = new CIBlockElement;

    $arSelect = Array("ID", "NAME", "PROPERTY_ARTICLE_XML");
    $arFilter = Array("IBLOCK_ID"=>$iblockId, "PROPERTY_ARTICLE_XML"=>array_keys($arGoods));
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNext())
    {
			$arGoods[$ob['PROPERTY_ARTICLE_XML_VALUE']]['ID'] = $ob['ID'];
    }

		foreach ($arGoods as $key => $value) {
			//count sub step elements
			$num_el++;

			if($arData['IMPORT_NEXT_ID']<$num_el && $current_step_count_el<$arParams['COUNT_ONE_STEP_IMPORT_EL']) {
				$current_step_count_el++;
				$last_import_id=$num_el;

				if($value['ID'] <= 0) {
					//if its new element

					$value['ARTIKUL']=$value['PROPERTY_VALUES'][$elementArticleId];
					$value['IBLOCK_ID'] = $iblockId;
		      $value['ACTIVE'] = 'Y';

					$arResult_import=WsImportTech::AddElement($value, $arParams);
					$what_to_do='new';
					$_SESSTION["ADD_EL"]++;
				}
				else {
					$arResult_import=WsImportTech::UpdateElement($value['ID'], $value['PROPERTY_VALUES'], $arParams);
					$what_to_do='update';
				}
			}
		}

		if ($current_step_count_el>0) {
			$arResult['WAIT_THIS_STEP']='Y';

			WsImport::InitData(
				'',
				array(
					'IMPORT_NEXT_ID'=>$last_import_id,
					'LAST_MAX_ARTIKUL'=>$last_max_art,
					'SUB_STEP'=>$arData['SUB_STEP']+1,
				)
			);
		}
		else {
			WsImport::InitData($arParams, array('IMPORT_NEXT_ID'=>0, 'LAST_MAX_ARTIKUL'=>0));
		}

		return $arResult;
	}

	//clear results of import and show result
	function Step3($arParams, $arData) {
		$arResult['FINISH']='Y';
		return $arResult;
	}
}
?>