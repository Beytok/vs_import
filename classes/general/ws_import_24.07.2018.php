<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

global $DB, $MESS, $APPLICATION;

if (!class_exists('CMainPage')) {
	require($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/mainpage.php");
	CModule::IncludeModule("main");
}

CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");

$rsSites = CSite::GetByID(CMainPage::GetSiteByHost());
$arSite = $rsSites->Fetch();

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/ws_import/lang/'.$arSite['LANGUAGE_ID'].'/classes/general/ws_import.php');

class WsImportTech{

	function charTranslit($value){
		$arParams = array("replace_space"=>"_","replace_other"=>"_", 'delete_repeat_replace'=>'true', 'max_len'=>'150', 'change_case'=>'L');
		return Cutil::translit($value, "ru", $arParams);
	}

	function GenerateNewMaxArt($last_max_art) {
		$new_max_art_numberic=$last_max_art['numberic_ARTIKUL']+1;

		$new_max_art=array(
			'ARTIKUL'=>str_replace(
				$last_max_art['numberic_ARTIKUL'],
				$new_max_art_numberic,
				$last_max_art['ARTIKUL']
			),
			'numberic_ARTIKUL'=>$new_max_art_numberic
		);
		return $new_max_art;
	}

	function GenerateAddInfo($arRes, $arParams, $what_generate='') {

		switch($what_generate) {
			case 'SECTION3_CODE':
				return WsImportTech::charTranslit($arRes['SECTION2']).'_'.WsImportTech::charTranslit($arRes['SECTION3']);
			break;
			case 'SECTION2_CODE':
				return WsImportTech::charTranslit($arRes['SECTION2']);
			break;
			case 'ELEMENT_CODE':
				return WsImportTech::charTranslit($arRes['ARTIKUL']);
			break;
			case 'CURRENCY':
				return $arParams['DEFAULT_CURRENCY'];
			break;
		}
	}

	function WriteToFile($file_name, $data_array, $write_params){
		if(is_array($data_array)) {
			$arParams=WsImport::Init();

			$h_new = fopen($file_name, $write_params);

			foreach($data_array as $new_line) {
				$string_data.=implode($arParams['DELIMITER'], $new_line);
				$string_data.="\n";
			}

			fwrite($h_new, $string_data);
			fclose($h_new);
			chmod($h_new, 0777);
		}
	}

	function GetParrentSection($section_id) {
		global $glob_sections_array;
		if (array_key_exists($section_id, $glob_sections_array)) {
			$ar_res_r=$glob_sections_array[$section_id];
		}
		else {
			$res = CIBlockSection::GetByID($section_id);

			if($ar_res = $res->GetNext()) {
				$ar_res_r = $ar_res;
				$glob_sections_array[$section_id]=array(
					'ID'=>$ar_res_r['ID'],
					'IBLOCK_SECTION_ID'=>$ar_res_r['IBLOCK_SECTION_ID'],
					'NAME'=>$ar_res_r['NAME'],
					'XML_ID'=>$ar_res_r['XML_ID'],
					'CODE'=>$ar_res_r['CODE'],
				);
			}
		}

		return(
			array(
				'ID'=>$ar_res_r['ID'],
				'IBLOCK_SECTION_ID'=>$ar_res_r['IBLOCK_SECTION_ID'],
				'NAME'=>$ar_res_r['NAME'],
				'XML_ID'=>$ar_res_r['XML_ID'],
				'CODE'=>$ar_res_r['CODE'],
			)
		);
	}


	//added by version 0.3
	function ImportSaleInfo($offer, $el_id, $what_to_do) {
		$arParams=(is_array($arParams))?$arParams:WsImport::Init();

		$arProduct = array(
			'ID' => $el_id,
			'PRODUCT_ID' => $el_id,
			'QUANTITY'=>1,
			'VAT_INCLUDED' => 'Y',
			'QUANTITY_TRACE' => 'N'
		);

		if($offer['PRICE']>0) {
			$offer['PRICE'] = str_replace(",",".",$offer['PRICE']);
			$PRICES[$arParams['PRICE_ID']] = array(
				'PRODUCT_ID' => $el_id,
				'PRICE' => ceil($offer['PRICE']),
				'CURRENCY' => ($offer['CURRENCY']=='')?$arParams['DEFAULT_CURRENCY']:$offer['CURRENCY'],
				'CATALOG_GROUP_ID'=> $arParams['PRICE_ID']
			);
		}
		CCatalogProduct::Add($arProduct);

		if($what_to_do=='update') {
			$db_res = CPrice::GetList(
				array(),
				array("PRODUCT_ID" => $el_id)
			);
			while($ar_res = $db_res->Fetch()) {
				if(isset($PRICES[$ar_res['CATALOG_GROUP_ID']])) {
					CPrice::Update($ar_res['ID'], $PRICES[$ar_res['CATALOG_GROUP_ID']], true);
					unset($PRICES[$ar_res['CATALOG_GROUP_ID']]);
					$had_price=1;
				}
			}
		}

		if($had_price!=1) {
			foreach($PRICES as $PRICE) {
				CPrice::Add($PRICE, true);
			}
		}
	}

	//added discount version 0.1
	function DiscountAdd($arItem, $el_id) {

		$dis = intval($arItem["DISCOUNT"]);

		//discount>0
		if($dis>0){
			$arFilter = array("NAME"=>$arItem["ARTIKUL"],"SITE_ID"=>"s1");

			$dbResultList = CCatalogDiscount::GetList(
				array(),
				$arFilter,
				false,
				false,
				array("ID")
			);

			$hasdis=0;

			//check dis
			if($arDisRes = $dbResultList->Fetch()){
				$hasdis=1;
			}

			$arFields = array(
				"NAME" => $arItem["ARTIKUL"],
				"VALUE" => $dis,
				"VALUE_TYPE" => "F",
				"SITE_ID" => "s1",
				"CURRENCY" => "RUB",
				"CONDITIONS" => array(
					"CLASS_ID" => "CondGroup",
					"DATA" => Array(
						"All" => "OR",
						"True" => "True"
					),
					"CHILDREN" => Array(
						array(
							"CLASS_ID" => "CondIBElement",
							"DATA" => Array(
								"logic" => "Equal",
								"value" => $el_id
							)
						)
					)
				)
			);

			//update
			if($hasdis==1){
				$res = CCatalogDiscount::Update($arDisRes["ID"], $arFields);
			}
			//add
			else{
				$ID = CCatalogDiscount::Add($arFields);
			}
		}
	}


	function AddSection($arElement_array, $IBLOCK_SECTION_ID='', $DEPTH_LEVEL, $arParams) {
		$bs = new CIBlockSection;
		$arFields = Array(
			"ACTIVE" => 'Y',
			"IBLOCK_ID" => $arParams['IBLOCK_ID'],
			"NAME" => $arElement_array['SECTION'.$DEPTH_LEVEL],
			"SORT" => '500',
			'CODE' => WsImportTech::GenerateAddInfo($arElement_array, $arParams, 'SECTION'.$DEPTH_LEVEL.'_CODE'),
		);

		if($IBLOCK_SECTION_ID>0) {
			$arFields['IBLOCK_SECTION_ID']=$IBLOCK_SECTION_ID;
		}
		if($ID = $bs->Add($arFields)) {
			$arResult['OK']=1;
			$arResult['ID']=$ID;
		}
		else {
			$arResult['ERROR']=$bs->LAST_ERROR;
		}

		return $arResult;
	}

	function GetCreateCurrentSectionID($arElement_array, $arParams) {

		$arElement_array['SECTION1'] = trim($arElement_array['SECTION1']);

		//get first level
		$arFilter = array(
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'NAME' => $arElement_array['SECTION1'],
			'DEPTH_LEVEL'=>1,
		);
		$rsSect = CIBlockSection::GetList(
			array('ID' => 'ASC'),
			$arFilter,
			false,
			array('ID'),
			array("nTopCount"=>1)
		);
		while ($arSect = $rsSect->GetNext()) {
			$FIRST_LEVEL_ID=$arSect['ID'];
		}
		if(!$FIRST_LEVEL_ID)  {
			$ADD_NEW=WsImportTech::AddSection($arElement_array, '', 1, $arParams);
			if($ADD_NEW['OK']){
				$FIRST_LEVEL_ID=$ADD_NEW['ID'];
			}
		}

		$arElement_array['SECTION2'] = trim($arElement_array['SECTION2']);

		//get second level
		$arFilter = array(
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'NAME' => $arElement_array['SECTION2'],
			'DEPTH_LEVEL'=>2,
			'SECTION_ID'=>$FIRST_LEVEL_ID,
		);
		$rsSect = CIBlockSection::GetList(
			array('ID' => 'ASC'),
			$arFilter,
			false,
			array('ID'),
			array("nTopCount"=>1)
		);
		while ($arSect = $rsSect->GetNext()) {
			$SECOND_LEVEL_ID=$arSect['ID'];
		}
		if(!$SECOND_LEVEL_ID)  {
			$ADD_NEW=WsImportTech::AddSection($arElement_array, $FIRST_LEVEL_ID, 2, $arParams);
			if($ADD_NEW['OK']){
				$SECOND_LEVEL_ID=$ADD_NEW['ID'];
			}
		}

		//get third level

		$arElement_array['SECTION3'] = trim($arElement_array['SECTION3']);

		$arFilter = array(
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			'NAME' => $arElement_array['SECTION3'],
			'DEPTH_LEVEL'=>3,
			'SECTION_ID'=>$SECOND_LEVEL_ID,
		);
		$rsSect = CIBlockSection::GetList(
			array('ID' => 'ASC'),
			$arFilter,
			false,
			array('ID'),
			array("nTopCount"=>1)
		);
		while ($arSect = $rsSect->GetNext()) {
			$THIRD_LEVEL_ID=$arSect['ID'];
		}
		if(!$THIRD_LEVEL_ID)  {
			$ADD_NEW=WsImportTech::AddSection($arElement_array, $SECOND_LEVEL_ID, 3, $arParams);
			if($ADD_NEW['OK']){
				$THIRD_LEVEL_ID=$ADD_NEW['ID'];
			}
		}

		if($THIRD_LEVEL_ID>0) {
			return $THIRD_LEVEL_ID;
		}
	}

	function CreateElementArray($arElement_array, $arParams) {
		$section_id=WsImportTech::GetCreateCurrentSectionID($arElement_array, $arParams);

		$arResult = Array(
			'CODE'=>WsImportTech::GenerateAddInfo($arElement_array, $arParams, 'ELEMENT_CODE'),
			'XML_ID' => $arElement_array['ARTIKUL'],
			'IBLOCK_SECTION' => $section_id,
			'IBLOCK_ID'=>$arParams['IBLOCK_ID'],
			'ACTIVE'=>'Y',
			'NAME'=>$arElement_array['NAME'],
		);

		return $arResult;
	}

	function UpdateElement($element_id, $arRes_clean, $arParams) {

		foreach($arParams['WHAT_WE_SHOOUD_UPDATE_IN_ELEMENTS']['PARAMS'] as $key_param) {
			if($arRes_clean[$key_param]!='') {
				CIBlockElement::SetPropertyValuesEx($element_id, $arParams['IBLOCK_ID'], array($key_param => $arRes_clean[$key_param]));
			}
		}

		//���������� ��� �������

		$arResult['OK']=1;
		$arResult['ID']=$element_id;
		return $arResult;
	}

	function AddElement($arRes_clean, $arParams) {
		$el = new CIBlockElement;

		foreach($arRes_clean as $key => $val){
			$arRes_clean[$key] = trim($val);
		}

		if($res = $el->Add(WsImportTech::CreateElementArray($arRes_clean, $arParams))) {
			$arResult['OK']=1;
			$arResult['ID']=$res;

			foreach($arParams['WHAT_WE_SHOOUD_ADD_IN_ELEMENTS']['PARAMS'] as $key_param) {

				if($arRes_clean[$key_param]!='') {
					CIBlockElement::SetPropertyValuesEx($arResult['ID'], $arParams['IBLOCK_ID'], array($key_param => $arRes_clean[$key_param]));
				}
			}
		}
		else {
			echo $el->LAST_ERROR;
		}

		return $arResult;
	}

	function GetNumArticle($arData) {
		$to_remove=array(
			'A', '�', '�'
		);
		return ltrim(str_replace($to_remove, '', $arData), '0');
	}
}

Class WsImport {
	function Init($arParams='') {
		$arParams['IBLOCK_ID']=($arParams['IBLOCK_ID']=='')?WsImport::GetModuleParam('catalog_iblock_id'):$arParams['IBLOCK_ID'];
		$arParams['LAST_EXPORT_TIME'] = WsImport::GetModuleParam('last_export_start_time');

		$arParams['DEFAULT_FOLDER'] = $_SERVER['DOCUMENT_ROOT'].'/upload/import_ws/';

		$arParams['DELIMITER'] = '|';
		$arParams['CSV_FIELD_TYPE'] = 'R';

		$arParams['FILE_WITH_PARAMS'] = $arParams['DEFAULT_FOLDER'].'settings.txt';

		$arParams['DEFAULT_EXPORT_CSV'] = $arParams['DEFAULT_FOLDER'].'export.csv';
		$arParams['DEFAULT_IMPORT_CSV'] = $arParams['DEFAULT_FOLDER'].'import.csv';
		$arParams['DEFAULT_EXISTS_IMPORT_CSV'] = $arParams['DEFAULT_FOLDER'].'import_had.csv';
		$arParams['DEFAULT_NEW_IMPORT_CSV'] = $arParams['DEFAULT_FOLDER'].'import_new.csv';

		$arParams['IMPORT_FIELDS_NUM']=array(
			'SECTION1'=>'0',
			'SECTION2'=>'1',
			'SECTION3'=>'2',
			'ARTIKUL'=>'3',
			'NAME'=>'4',
			'RAZMER'=>'5',
			'EXT_CODE'=>'6',
			'VES'=>'7',
			'LENGHT_IN_BOX'=>'8',
			'KOLICHESTVO'=>'9',
			'EDINICA'=>'10',
			'SELLER'=>'11',
			'BAUSERVICE_ID'=>'12',
			'PRICE'=>'13',
			'CURRENCY'=>'14',
			'DISCOUNT'=>'15',
			'POVERHNOST'=>'16',
			'MATERIAL_TILE'=>'17',
			'DESTINATION_TILE'=>'18',
			'PLITA_PO_DESIGN'=>'19',
			'PLITA_PO_RAZMER'=>'20',
		);

		$arParams['COUNT_ONE_STEP_IMPORT_EL']=10;

		//sale info
		$arParams['PRICE_ID'] = 1;
		$arParams['DEFAULT_CURRENCY']='RUB';

		//to update element
		$arParams['WHAT_WE_SHOOUD_UPDATE_IN_ELEMENTS']=array(
			'PARAMS'=>array(
				'EDINICA',
				'EXT_CODE',
				'LENGHT_IN_BOX',
				'VES',
				'SELLER',
				'BAUSERVICE_ID',
				'POVERHNOST',
				'MATERIAL_TILE',
				'DESTINATION_TILE',
				'PLITA_PO_DESIGN',
				'PLITA_PO_RAZMER',
			),
		);

		//to add element
		$arParams['WHAT_WE_SHOOUD_ADD_IN_ELEMENTS']=array(
			'PARAMS'=>array(
				'ARTIKUL',
				'RAZMER',
				'EXT_CODE',
				'VES',
				'METRAZ_K',
				'KOLICHESTVO',
				'EDINICA',
				'SELLER',
				'BAUSERVICE_ID',
				'POVERHNOST',
				'LENGHT_IN_BOX',
				'MATERIAL_TILE',
				'DESTINATION_TILE',
				'PLITA_PO_DESIGN',
				'PLITA_PO_RAZMER',
			),
		);

		return $arParams;
	}

	function InitData($arParams, $addParams='', $clearParams='') {

		$arParams=(is_array($arParams))?$arParams:WsImport::Init();

		if(file_exists($arParams['FILE_WITH_PARAMS'])) {
			$arData=unserialize(file_get_contents($arParams['FILE_WITH_PARAMS']));
			if(!is_array($arData)){
				$arData=array();
			}
		}

		foreach($_REQUEST['WS_IMPORT'][$arParams['IBLOCK_ID']] as $name_v => $value) {
			$arData[$arParams['IBLOCK_ID']][$name_v]=$value;
		}

		if($clearParams=='all') {
			$arData[$arParams['IBLOCK_ID']]='';
			unset($arData);
		}
		elseif(is_array($clearParams)) {
			foreach($clearParams as $clear_key) {
				unset($arData[$arParams['IBLOCK_ID']][$clear_key]);
			}
		}

		if(is_array($addParams)) {
			foreach($addParams as $key_param => $value_param) {
				$arData[$arParams['IBLOCK_ID']][$key_param] = $value_param;
			}

			$add_it=1;
		}

		$h_new = fopen($arParams['FILE_WITH_PARAMS'], 'w');
		fwrite($h_new, serialize($arData));
		fclose($h_new);
		chmod($h_new, 0777);

		if(is_array($arData[$arParams['IBLOCK_ID']])) {
			foreach($arData[$arParams['IBLOCK_ID']] as $key_n => $value) {
				$arData[$key_n]=$value;
			}
		}
		return $arData;
	}

	function GetModuleParam($param_name){
		return 	COption::GetOptionString('ws_import', $param_name);
	}

	function SetModuleParam($param_name, $param_value){
		return COption::SetOptionString('ws_import', $param_name, $param_value);
	}

	function Action($arParams) {

		// just for debug)
		// WsImport::InitData('', '', 'all');
		// die();

		$arParams=WsImport::Init($arParams);
		$arData=WsImport::InitData($arParams);

		//checking exists step
		if(!method_exists('WsImport','Step'.$arData['STEP'])) {
			$arData['STEP']=0;
			WsImport::InitData(
				$arParams,
				array(
					'SUB_STEP'=>0,
					'STEP'=>$arData['STEP']
				)
			);
		}


		//checking global errors
		switch($arData['STEP']) {
			case '2':

			break;
			case '1':

			break;
			case '0':
				//check not had uncomplete import
				if($arParams['LAST_EXPORT_TIME']>0 && $arData['CONFIRM_WITH_ERRORS']!='on') {
					$arParams['ERRORS'][]=GetMessage('WAS_STARTED_EXPORT').date('H:i:s d.m.Y', $arParams['LAST_EXPORT_TIME']);
				}
			break;
		}


		//start all functions
		$func_name='Step'.$arData['STEP'];

		$arResult=WsImport::$func_name($arParams, $arData);

		if(is_array($arResult['WARNINGS'])) {
			if(is_array($arData['WARNINGS'])) {
				$arResult['WARNINGS']=array_merge($arData['WARNINGS'], $arResult['WARNINGS']);
			}

			WsImport::InitData(
				'',
				array(
					'WARNINGS'=>$arResult['WARNINGS']
				)
			);
		}

		if($arData['FINISH']=='Y' || $arResult['FINISH']=='Y') {
			WsImport::SetModuleParam('last_export_start_time', 0);
			WsImport::InitData('', '', 'all');
		}

		if(!is_array($arParams['ERRORS']) && $arData['FINISH']!='Y' && $arResult['FINISH']!='Y' && $arResult['WAIT_THIS']!='Y') {
			if(!$arResult['WAIT_THIS_STEP']) {
				WsImport::InitData(
					$arParams,
					array(
						'SUB_STEP'=>0,
						'STEP'=>($arData['STEP']+1)
					)
				);
			}
			$arResult['RELOAD_PAGE']=1;
		}

		return array(
			'PARAMS'=>$arParams,
			'DATA'=>WsImport::InitData($arParams),
			'RESULT'=>$arResult
		);
	}

	//get user file, get all info from user
	function Step0($arParams, $arData) {
		$arResult['WAIT_THIS']='Y';

		$uploadfile = $arParams['DEFAULT_IMPORT_CSV'];

		if (copy($_FILES['import_file']['tmp_name'], $uploadfile)) {
			unset($arResult['WAIT_THIS']);
			WsImport::SetModuleParam('last_export_start_time', time()); //mark what we alredy had started import
			$arResult['MESSAGE'][] = GetMessage('FILE_UPLOADED');
		}
		/* Add */
		elseif (is_numeric($arParams["IBLOCK_ID"]) &&
		        $_REQUEST["WS_IMPORT"][$arParams["IBLOCK_ID"]]["GET_USER_EXPORT"] == 'on')
		{
	        unset($arResult['WAIT_THIS']);
			WsImport::SetModuleParam('last_export_start_time', time()); //mark what we alredy had started import
		}
		/* Add */

		return $arResult;
	}

	//create copy of db
	function Step1($arParams, $arData) {
		return $arResult;
	}

	//get exists elements and split file
	function Step2($arParams, $arData) {
		if($arData['SUB_STEP']<1) {
			$h_new = fopen($arParams['DEFAULT_EXPORT_CSV'], 'w');

			$string_title=array(
				'�������',
				'ID',
				'��� ����������',
				'����',
			);

			if(isset($arData['GET_USER_EXPORT'])) {
				$string_title=array_merge(
					$string_title,
					array(
						'����������',
						'������������',
						'����',
						'������',
						'��������',
						'�������������',
						'������',
						'���������',
						'��� ����������',
					)
				);
			}

			$string_title=implode($arParams['DELIMITER'], $string_title)."\n";

			fwrite($h_new, $string_title);
			fclose($h_new);
			chmod($h_new, 0777);

			WsImport::InitData(
				$arParams,
				array(
					'SUB_STEP'=>1,
					'EXPORT_LAST_ID'=>0,
				)
			);

			$arResult['WAIT_THIS_STEP']='Y';
		}
		else {

			$num_el=0;
			$h_new = fopen($arParams['DEFAULT_EXPORT_CSV'], 'a+');

			CModule::IncludeModule('iblock');
			CModule::IncludeModule('catalog');

			$arFilter = Array('>ID'=>$arData['EXPORT_LAST_ID'], 'IBLOCK_ID'=>$arParams['IBLOCK_ID']);

			$res = CIBlockElement::GetList(
				Array("ID"=>"ASC"),
				$arFilter,
				false,
				array('nTopCount'=>1000),
				Array(
					'ID',
					'ACTIVE',
					'XML_ID',
					'PROPERTY_ARTIKUL',
					'NAME',
					"CODE",
					'CATALOG_GROUP_1',
					'PROPERTY_RAZMER',
					'PROPERTY_SELLER',
					'IBLOCK_SECTION_ID',
					'PROPERTY_EXT_CODE'
				)
			);


			$objElement = new CIBlockElement();
			while($ar_fields = $res->GetNext()) {

				$num_el++;

				$ar_fields['PROPERTY_ARTIKUL_VALUE'] = trim($ar_fields['PROPERTY_ARTIKUL_VALUE']);

				/* CheckARTIKUL */
				$arLoadProductArray = array();

				if(!empty($ar_fields['PROPERTY_ARTIKUL_VALUE']))
				{
					if ($ar_fields['PROPERTY_ARTIKUL_VALUE'] != $ar_fields['XML_ID'])
					{
				       $arLoadProductArray["XML_ID"] = $ar_fields['PROPERTY_ARTIKUL_VALUE'];
					}

					$realCode = strtolower(WsImportTech::charTranslit(trim($ar_fields['PROPERTY_ARTIKUL_VALUE'])));

					if (!empty($realCode) &&
					    (empty($ar_fields["CODE"]) ||
						 $realCode != $ar_fields["CODE"]))
					{
					  $arLoadProductArray["CODE"] = $realCode;
					}
				}

				if (count($arLoadProductArray) > 0)
				{
			       $objElement->Update($ar_fields['ID'],
				                       $arLoadProductArray);
				}
				/* CheckARTIKUL */


				$last_exported_el = $ar_fields['ID'];

				$string_data=array(
					$ar_fields['PROPERTY_ARTIKUL_VALUE'],
					html_entity_decode($ar_fields['ID']),
					$ar_fields['PROPERTY_EXT_CODE_VALUE'],
					$ar_fields['CATALOG_PRICE_1'],
				);

				if(isset($arData['GET_USER_EXPORT'])) {
					$coll_arr=WsImportTech::GetParrentSection($ar_fields['IBLOCK_SECTION_ID']);
					$vendor_arr=WsImportTech::GetParrentSection($coll_arr['IBLOCK_SECTION_ID']);
					$country_arr=WsImportTech::GetParrentSection($vendor_arr['IBLOCK_SECTION_ID']);

					$string_data=array_merge(
						$string_data,
						array(
							$ar_fields['ACTIVE'],
							$ar_fields['NAME'],
							$ar_fields['CATALOG_PRICE_1'],
							$ar_fields['PROPERTY_RAZMER_VALUE'],
							$coll_arr['NAME'],
							$vendor_arr['NAME'],
							$country_arr['NAME'],
							$ar_fields['PROPERTY_SELLER_VALUE'],
							$ar_fields['PROPERTY_EXT_CODE_VALUE']
						)
					);
				}

				$string_data_full=implode($arParams['DELIMITER'], $string_data)."\n";

				fwrite(
					$h_new,
					$string_data_full
				);
			}
			fclose($h_new);
			chmod($h_new, 0777);

			if ($num_el<=0) {
				if(isset($arData['GET_USER_EXPORT'])) {
					$arResult['FINISH']='Y';
					$arResult['WS_MESSAGE'][]='DOWNLOAD_EXPORT';
				}
			}
		}

		if ($num_el>0) {
			$arResult['WAIT_THIS_STEP']='Y';
			WsImport::InitData(
				'',
				array(
					'EXPORT_LAST_ID'=>$last_exported_el,
					'SUB_STEP'=>$arData['SUB_STEP']+1,
				)
			);
		}
		else {
			WsImport::InitData($arParams, array('EXPORT_LAST_ID'=>0));
		}
		return $arResult;
	}

	//split uploaded file to exists and not exists elements
	function Step3($arParams, $arData) {
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");

		//get exists data(for checking)
		$csvFile_export = new CCSVData();
		$csvFile_export->LoadFile($arParams['DEFAULT_EXPORT_CSV']);
		$csvFile_export->SetDelimiter($arParams['DELIMITER']);
		$csvFile_export->SetFieldsType($arParams['CSV_FIELD_TYPE']);

		$last_max_art=(isset($arData['LAST_MAX_ARTIKUL']))?$arData['LAST_MAX_ARTIKUL']:0;

		while ($arRes = $csvFile_export->Fetch()) {

			$ARTIKUL = trim($arRes[0]);
			$id = trim($arRes[1]);

			$id_seler = trim($arRes[2]);
			$priceElem = trim($arRes[3]);

			$numberic_ARTIKUL = WsImportTech::GetNumArticle($ARTIKUL);

			if($numberic_ARTIKUL>$last_max_art['numberic_ARTIKUL'] && is_numeric($numberic_ARTIKUL)) {
				$last_max_art=array(
					'ARTIKUL'=>$ARTIKUL,
					'numberic_ARTIKUL'=>$numberic_ARTIKUL,
				);
			}

			if($ARTIKUL!='') {
				$exists_ARTIKULs[$ARTIKUL] = $id;
				if($id_seler!=''){
					$exists_Elements[$ARTIKUL][$ARTIKUL] = $id;
					$exists_Elements[$ARTIKUL][$id_seler] = $priceElem;
				}
			}
		}

		//generate two new files
		$csvFile = new CCSVData();
		$csvFile->LoadFile($arParams['DEFAULT_IMPORT_CSV']);
		$csvFile->SetDelimiter($arParams['DELIMITER']);
		$csvFile->SetFieldsType($arParams['CSV_FIELD_TYPE']);
		$csvFile->SetFirstHeader(true);

		$num_el = $current_step_count_el = 0;

		$objElement = new CIBlockElement;

		while ($arRes = $csvFile->Fetch()) {
			//count sub step elements
			$num_el++;

			/* CheckARTIKUL */

			if($arData['IMPORT_NEXT_ID']<$num_el && $current_step_count_el<$arParams['COUNT_ONE_STEP_IMPORT_EL']) {
				$current_step_count_el++;
				$last_import_id=$num_el;
				$arResult_import = array();
				$arRes_clean = array();

				//clear getted data
				foreach($arRes as $key_res => $value) {
					$arRes[$key_res]=trim($value);
				}

				//create element array
				foreach($arParams['IMPORT_FIELDS_NUM'] as $key_num => $id_num) {
					if($arRes[$id_num]!='') {
						$arRes_clean[$key_num]=trim($arRes[$id_num]);
					}
				}


				foreach($arRes_clean as $keyFields => $arValue){
					switch($keyFields){
						case "MATERIAL_TILE": $iblock = 32; break;
						case "DESTINATION_TILE": $iblock = 33; break;
						case "PLITA_PO_DESIGN": $iblock = 68; break;
						case "PLITA_PO_RAZMER": $iblock = 69; break;
						default: $iblock = 0; break;
					}

					if(empty($arValue) || $iblock==0){
						continue;
					}


					$arValue = explode(",",$arValue);
					$arPropertyValue = array();
					$bs = new CIBlockSection;
					foreach($arValue as $keyV => $nameSection){
						$nameSection = trim($nameSection);

						$filter = array("IBLOCK_ID"=>$iblock,"NAME"=>$nameSection);

						$arSection = CIBlockSection::GetList(array(),$filter,false,array("ID","NAME"),array("nTopCount"=>1))->Fetch();

						if($arSection["ID"]>0){
							$arPropertyValue[] = $arSection["ID"];
						}
						else{
							$arFieldsSec = array(
								"NAME" => $nameSection,
								"ACTIVE" => "Y",
								"IBLOCK_ID" => $iblock,
								"CODE" => WsImportTech::charTranslit($nameSection),
							);

							$arSection = $bs->Add($arFieldsSec);
							if($arSection>0){
								$arPropertyValue[] = $arSection;
							}

						}
					}
					$arRes_clean[$keyFields] = $arPropertyValue;
				}




				if($arRes_clean['ARTIKUL']=='' && $arRes_clean['EXT_CODE']!='') {
					foreach($exists_Elements as $key => $ext_ELEMENT) {
						if(array_key_exists($arRes_clean['EXT_CODE'], $ext_ELEMENT)) {
							$arRes_clean['ARTIKUL'] = $key;
						}
					}
				}

				if($arRes_clean['ARTIKUL']=='') {
					//if its new element
					$last_max_art = WsImportTech::GenerateNewMaxArt($last_max_art);
					$arRes_clean['ARTIKUL']=$last_max_art['ARTIKUL'];

					$arResult_import=WsImportTech::AddElement($arRes_clean, $arParams);
					$what_to_do='new';
				}
				else {

					if($arData["CHANGE_NAME"]=="on" && $arRes[4]!=""){
						$arLoadProductArray["NAME"] = trim($arRes[4]);
					}

					if (count($arLoadProductArray) > 0)
					{

					   $objElement->Update($exists_ARTIKULs[$arRes_clean['ARTIKUL']],
										   $arLoadProductArray);
					}

					if(isset($exists_ARTIKULs[$arRes_clean['ARTIKUL']])) {
						//if its existing element
						$arResult_import=WsImportTech::UpdateElement($exists_ARTIKULs[$arRes_clean['ARTIKUL']], $arRes_clean, $arParams);

						$what_to_do='update';
					}
					else {
						$arResult['WARNINGS'][] = GetMessage('NOT_FOUND_AND_NOT_EXPORTED_ARTIKUL').$arRes_clean['ARTIKUL'];
					}
				}


				if($arResult_import['OK']) {
					if($arRes_clean["PRICE"]!=''){
						$arResult_import['SALE']=WsImportTech::ImportSaleInfo($arRes_clean, $arResult_import['ID'], $what_to_do);
					}
				}

				//add discount
				if(intval($arRes_clean["DISCOUNT"])>0){
					$arResult_import['DISCOUNT']=WsImportTech::DiscountAdd($arRes_clean, $arResult_import['ID']);
				}

			}
		}

		if ($current_step_count_el>0) {
			$arResult['WAIT_THIS_STEP']='Y';

			WsImport::InitData(
				'',
				array(
					'IMPORT_NEXT_ID'=>$last_import_id,
					'LAST_MAX_ARTIKUL'=>$last_max_art,
					'SUB_STEP'=>$arData['SUB_STEP']+1,
				)
			);
		}
		else {
			WsImport::InitData($arParams, array('IMPORT_NEXT_ID'=>0, 'LAST_MAX_ARTIKUL'=>0));
		}

		//$arResult['WAIT_THIS']='Y';
		return $arResult;
	}

	//clear results of import and show result
	function Step4($arParams, $arData) {
		$arResult['FINISH']='Y';
		return $arResult;
	}
}
?>